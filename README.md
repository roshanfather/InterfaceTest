# interfaceTest
   Python开发接口测试框架，将结果以html的形式进行结果分析
    
# config.ini文件配置
 需要用户对根据配置文件的内容进行填写
```python
 
[DATABASE]
host = 10.10.12.191
username = XXXXXXXXX
password = XXXXXX
port = XXXX
database = XXXX

[HTTPS]
baseurl = https://10.10.12.196
port = 443
timeout = 5.0

[EMAIL]
mail_host = smtp.exmail.qq.com
mail_user = XXXXXXXX
mail_pass = XXXXXX
mail_port = 25
sender = XXXXXX
receiver = XXXXXXX
subject = APP-SPC800 接口自动化测试结果
content = "APP-SPC800 接口自动化测试报告"
on_off = 1  # 1:to send test result report; 0: not send test result report
```

#####测试用例文件，必须是以test_开头，才能检测到文件并执行测试用例


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
